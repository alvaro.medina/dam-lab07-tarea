import React, { Component } from 'react';
import { View, Text, Button, Image } from 'react-native';

import { NavigationContainer, TabActions } from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import TransferenceFirst from './app/component/TransferenceFirst';
import TransferenceSecond from './app/component/TransferenceSecond';
import TransferenceThird from './app/component/TransferenceThird';

const TransferenceStack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function TransferenceStackScreen() {
  return (
    <TransferenceStack.Navigator>
      <TransferenceStack.Screen
        name="First"
        component={TransferenceFirst}
        options={({ navigation, route }) => ({
          title: 'Ingrese sus Datos',
        })}

      />
      <TransferenceStack.Screen
        name="Second"
        component={TransferenceSecond}
        options={({ navigation, route }) => ({
          title: 'Confirmación de Datos',
        })}
      />
      <TransferenceStack.Screen
        name="Third"
        component={TransferenceThird}
        options={({ navigation, route }) => ({
          title: 'Exito en la Transferencia',
        })}
      />
    </TransferenceStack.Navigator>
  );
}
function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Details"
        onPress={() => navigation.navigate('Details')}
      />
      <Text></Text>
      <Button
        title="Replace Details"
        onPress={() => navigation.replace('Details')}
      />
    </View>
  );
}
function NuevoScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Nuevo Screen</Text>
      <Button
        title="Go to Details"
        onPress={() =>
          navigation.reset({
            index: 0,
            routes: [
              {
                name: 'Details',
              },
            ],
          })
        }
      />
    </View>
  );
}
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Home"
          tabBarOptions={{
            activeTintColor: '#e91e63',
          }}
          barStyle={{ backgroundColor: '#000000' }}>
          <Tab.Screen
            name="Home"
            component={HomeScreen}
            options={{
              tabBarLabel: 'Inicio',
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="home" color={color} size={25} />
              ),
            }}
          />

          <Tab.Screen
            name="Transference"
            component={TransferenceStackScreen}
            options={{
              tabBarLabel: 'Transference',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="bank" color={color} size={25} />
              ),
            }}
          />
          <Tab.Screen
            name="Nuevo"
            component={NuevoScreen}
            options={{
              tabBarLabel: 'Nuevo',
              tabBarIcon: ({ color, size }) => (
                <MaterialCommunityIcons name="bell" color={color} size={25} />
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}
