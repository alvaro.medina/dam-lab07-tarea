import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';

export default class TransferenceSecond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      importe: 0,
    };
  }

  render() {
    return (
      <LinearGradient colors={['#FFB300', '#FFB400', '#FFD740']} style={styles.container}>
        <AntDesign name="checkcircle" color={'#000000'} size={200}></AntDesign>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: '#000000' }]}
          onPress={() => this.props.navigation.navigate('First')}>
          <Text style={[styles.textButton, { color: 'white' }]}>ACEPTAR</Text>
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  button: {
    paddingLeft: 30,
    paddingRight: 30,
    padding: 5,
    borderRadius: 5,
    marginTop: 20,
  },
  textButton: {
    fontSize: 20,
  },
});
