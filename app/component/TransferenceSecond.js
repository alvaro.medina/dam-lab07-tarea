import React, {Component} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';

class TransferenceSecond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datos: this.props.route.params.datos,
    };
  }

  render() {
    return (
      <LinearGradient colors={['#FFB300', '#FFB400', '#FFD740']} style={styles.container}>
        <View>
          <Text style={styles.text}>Cuenta origen</Text>
          <TextInput
            editable={false}
            style={styles.textInput}
            value={this.state.datos.origin}
          />
          <Text style={styles.text}>Cuenta destino</Text>
          <TextInput
            editable={false}
            style={styles.textInput}
            value={this.state.datos.destine}
          />
          <Text style={styles.text}>Importe</Text>
          <TextInput
            editable={false}
            style={styles.textInput}
            value={this.state.datos.amount}
          />
          <Text style={styles.text}>Referencia</Text>
          <TextInput
            editable={false}
            style={styles.textInput}
            value={this.state.datos.reference}
          />
          <Text style={styles.text}>Fecha</Text>
          <TextInput
            editable={false}
            style={styles.textInput}
            value={this.state.datos.date}
          />
          <Text style={styles.text}>Mail</Text>
          <TextInput
            editable={false}
            style={styles.textInput}
            value={this.state.datos.mail ? 'SI' : 'NO'}
          />
          <View
            style={{
              flexDirection: 'row',
              marginTop: 20,
              justifyContent: 'space-evenly',
            }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('First')}
              style={[styles.button, {backgroundColor: '#E0E0E0'}]}>
              <Text style={[styles.textButton, {color: 'black'}]}>VOLVER</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, {backgroundColor: '#000000'}]}
              onPress={() => this.props.navigation.navigate('Third')}>
              <Text style={[styles.textButton, {color: 'white'}]}>
                CONFIRMAR
              </Text>
            </TouchableOpacity>
          </View>
        </View>
     </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    padding: 20,
  },
  text: {
    marginTop: 20,
  },
  textInput: {
    backgroundColor: 'white',
    borderBottomWidth: 1,
    padding: -10,
    color: 'black',
  },
  button: {
    paddingLeft: 30,
    paddingRight: 30,
    padding: 5,
    borderRadius: 5,
  },
  textButton: {
    fontSize: 20,
  },
});

export default TransferenceSecond;
